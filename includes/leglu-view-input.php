<?php
/**
 * Legislator Lookup Input View
 *
 * Constructs HTML for display of lookup input form
 * @package Legislator Lookup
 * @since 2017.06.06
 */

/**
 * class to render lookup input
 *
 * @package Legislator Lookup
 * @since 0.1
 */
class LegLuViewInput
{
  /**
   * renders HTML
   * @param  array $data associative array of data for HTML
   * @return string       the HTML output
   */
  public static function render( $data ){
    // TODO set the element id once
    $elements = array(
      array(
        'tag'          => 'input',
        'id'           => 'leglu-input-address',
        'type'         => 'text',
        'placeholder'  => (isset($data['placeholder'])) ? $data['placeholder'] : ''
      ),
      array(
        'tag' => 'input',
        'type'    => 'hidden',
        'name'    => 'lat',
        'value'   => ''
      ),
      array(
        'tag' => 'input',
        'type'    => 'hidden',
        'name'    => 'lon',
        'value'   => ''
      ),
      array(
        'tag' => 'input',
        'id' => 'leglu-input-submit',
        'type'    => 'submit',
        'value'   => (isset($data['submitValue'])) ? $data['submitValue'] : __('Submit','leglu')
      ),
      array(
        'tag'   => 'div',
        'id'    => 'leglu-input-spinner',
        'style' => 'display: none;'
      )
    );

    ob_start();
?>
    <div class="leglu-wrapper">
      <?php
        foreach ($elements as $attr) {
          echo '<' . $attr['tag'] . ' ' . join(' ', array_map(function($key) use ($attr)
            {
               if($attr[$key] == 'tag') return null;
               if(is_bool($attr[$key]))
               {
                  return $attr[$key] ? $key : '' ;
               }
               return $key . '="' . $attr[$key] . '"';
            }, array_keys($attr))) . '>';

          // check if tag name is self closing, if not close the tag
          $self_closing_input = array('input','br','img','embed');
          echo ( !in_array($attr['tag'], $self_closing_input) ) ? '</' . $attr['tag'] . '>' : '' ;
        }
      ?>
    </div>
<?php
    return ob_get_clean();
  }

  /**
   * Creates an element from array
   * @param  string $name element tag name
   * @param  array $attr associative array with attribute => value (or bool)
   * @param  string $content content to place between the element tags
   * @return string       HTML tag with attributes
   */
  public function buildHTMLelement($name = 'input',$attr, $content = false){
    $result = '<input '.join(' ', array_map(function($key) use ($attr)
      {
         if(is_bool($attr[$key]))
         {
            return $attr[$key] ? $key : '' ;
         }
         return $key . '="' . $attr[$key] . '"';
      }, array_keys($attr))) . '>';

    $result .= ($content) ? $content : '' ;

    // check if element name is self closing, if not close the element
    $self_closing_input = array('input','br','img','embed');
    $result .= (in_array($name, $self_closing_input)) ? '' : '</' . $name . '>' ;

    return $result;
  }
}
