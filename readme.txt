=== Legislator Lookup ===
Contributors: dhornbein
Donate link: http://goodgoodwork.io/
Tags: political, us, legislator lookup
Requires at least: 4.6
Tested up to: 4.8
Stable tag: 1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Allows visitors to look up their local state reps using Open State API data.

== Description ==

This plugin provides a set of shortcodes to build pages that look up US state representatives using Open State API data.

The `[legislator-lookup]` shortcode creates an address input field that uses Google Places API to auto complete addresses.

The `[legislator-results]` is used to display the results of the search.

Currently both shortcodes need to appear on the same page to function properly.

The `legislator-lookup` shortcode accepts two attributes:

- `placeholder` which changes the default text in the address input.
- `submitValue` which changes the default submit button text.

The `legislator-results` shortcode accepts one attribute:

- `accordion` which controls the representative result boxes appearing collapsed and opening one at a time. Default is `true`, set to `false` to disable.

The `legislator-results` is an "enclosing" tag, meaning it can be wrapped around content in the editor. Anything the shortcode
wraps will appear next to the results. This can be used to give users a script to follow or some context for what to do after they search.

This plugin requires a Google Maps Javascript API key and Open States API key to work.

If you would like to contribute please find the active repository here: https://gitlab.com/goodgoodwork/wp-plugin-legislator-lookup

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/legislator-lookup` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Legislator Lookup screen to configure the plugin
1. Get the Google Maps API Key here: https://developers.google.com/maps/documentation/javascript/get-api-key
1. Get the Open State API Key here: https://openstates.org/api/register/

== Screenshots ==

1. A look at the shortcodes in the visual editor.
2. After an address has been searched.

== Changelog ==

= 17.10.20 =
* Initial build
